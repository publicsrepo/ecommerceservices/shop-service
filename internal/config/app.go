package config

import (
	"shop-services/internal/delivery/http"
	"shop-services/internal/delivery/http/middleware"
	"shop-services/internal/delivery/http/route"
	"shop-services/internal/repository"
	"shop-services/internal/usecase"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type BootstrapConfig struct {
	DB       *gorm.DB
	App      *fiber.App
	Log      *logrus.Logger
	Validate *validator.Validate
	Config   *viper.Viper
}

func Bootstrap(config *BootstrapConfig) {
	// setup repositories
	shopRepository := repository.NewShopRepository(config.Log)

	// setup use cases
	shopUseCase := usecase.NewShopUseCase(config.DB, config.Log, config.Validate, config.Config, shopRepository)

	// setup controller
	shopController := http.NewShopController(shopUseCase, config.Log)

	// setup middleware
	authMiddleware := middleware.NewAuth(shopUseCase)
	routeConfig := route.RouteConfig{
		App:            config.App,
		ShopController: shopController,
		AuthMiddleware: authMiddleware,
	}
	routeConfig.Setup()
}
