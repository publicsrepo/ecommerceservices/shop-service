package httpcall

import (
	"shop-services/pkg/httpcall"

	"github.com/gofiber/fiber/v2"
)

func GetProductDetail(ctx *fiber.Ctx) error {
	err := httpcall.SendRequest(ctx, httpcall.Request{
		Url:    "https://catfact.ninja/fact",
		Method: "GET",
	})
	return err

}
