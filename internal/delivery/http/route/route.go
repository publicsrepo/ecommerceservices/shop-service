package route

import (
	"shop-services/internal/delivery/http"

	"github.com/gofiber/fiber/v2"
)

type RouteConfig struct {
	App            *fiber.App
	ShopController *http.ShopController
	AuthMiddleware fiber.Handler
}

func (c *RouteConfig) Setup() {
	c.SetupConsoleRoute()
}

func (c *RouteConfig) SetupConsoleRoute() {
	c.App.Use(c.AuthMiddleware)

	c.App.Post("/api/v1/shop-management/shop", c.ShopController.CreateShop)
	c.App.Put("/api/v1/shop-management/shop", c.ShopController.UpdateShop)
	c.App.Delete("/api/v1/shop-management/shop", c.ShopController.DeleteShop)
	c.App.Get("/api/v1/shop-management/shops", c.ShopController.GetListShop)
	c.App.Get("/api/v1/shop-management/shop/:shop_id", c.ShopController.GetDetailShop)

	//TODO :use http call to calling product services
	//c.App.Post("/api/v1/shop-management/product", c.ShopController.CreateProduct)
	//c.App.Put("/api/v1/shop-management/product", c.ShopController.UpdateProduct)
	//c.App.Delete("/api/v1/shop-management/product", c.ShopController.DeleteProduct)
	//c.App.Get("/api/v1/shop-management/products", c.ShopController.GetListProduct)
	//c.App.Get("/api/v1/shop-management/product", c.ShopController.GetDetailProduct)
}
