package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"shop-services/internal/model/utils"
	"shop-services/pkg/helper"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/spf13/viper"
)

var secretKey = []byte("")

func VerifyConsoleToken(ctx *fiber.Ctx, config *viper.Viper) (bool, *utils.TokenClaimsData, *error) {
	isValid := false
	bearer := "Bearer "
	bearerToken := ctx.Get("Authorization")
	if len(bearerToken) == 0 || !strings.Contains(bearerToken, bearer) {
		err := errors.New("invalid or Incomplete Access Token")
		return false, nil, &err
	}

	conf, errConfig := GetConfigJwt(config)
	if errConfig != nil {
		return isValid, nil, errConfig
	}

	tokenStr := strings.TrimPrefix(bearerToken, bearer)
	token, _, err := new(jwt.Parser).ParseUnverified(tokenStr, jwt.MapClaims{})
	if err != nil {
		return isValid, nil, &err
	}

	claimsJSON, err := json.Marshal(token.Claims)
	if err != nil {
		return isValid, nil, &err
	}

	var dataMap utils.TokenClaimsData
	if err := json.Unmarshal(claimsJSON, &dataMap); err != nil {
		desc := fmt.Sprintf("Error unmarshaling JSON: %v", err)
		err := errors.New(desc)
		return isValid, nil, &err
	}
	timeNow := helper.GetCurrentTimeWithUTC()
	unixTime := time.Unix(int64(dataMap.Exp), 0)
	if !timeNow.Before(unixTime) {
		err := errors.New("expired Access Token")
		return false, nil, &err
	}
	if conf.Iss != dataMap.Iss {
		err := errors.New("invalid or Incomplete Access Token")
		return false, nil, &err
	}
	isValid = true
	return isValid, &dataMap, nil
}

func GetConfigJwt(config *viper.Viper) (*utils.JwtConfig, *error) {

	if config == nil {
		err := errors.New("config is not exist")
		return nil, &err
	}
	secret, errSecret := GetSecretKey(config)
	if errSecret != nil {
		return nil, errSecret
	}
	iss, errIss := GetIss(config)
	if errIss != nil {
		return nil, errIss
	}
	duration, errduration := GetDuration(config)
	if errduration != nil {
		return nil, errduration
	}
	return &utils.JwtConfig{
		SecretKey: *secret,
		Iss:       *iss,
		Duration:  *duration,
	}, nil
}

func GetSecretKey(config *viper.Viper) (*string, *error) {
	result := config.GetString("jwt.secret_key")
	if len(result) == 0 {
		err := errors.New("jwt.secret_key key not set in config")
		return nil, &err
	}
	return &result, nil
}
func GetIss(config *viper.Viper) (*string, *error) {
	result := config.GetString("jwt.iss")
	if len(result) == 0 {
		err := errors.New("jwt.iss key not set in config")
		return nil, &err
	}
	return &result, nil
}

func GetDuration(config *viper.Viper) (*utils.Duration, *error) {
	resultSeconds := config.GetInt64("jwt.duration.seconds")
	resultMinutes := config.GetInt64("jwt.duration.minutes")
	resultHours := config.GetInt64("jwt.duration.hours")

	return &utils.Duration{
		Second:  resultSeconds,
		Minutes: resultMinutes,
		Hour:    resultHours,
	}, nil
}
