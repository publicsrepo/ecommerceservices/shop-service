package http

import (
	"shop-services/internal/delivery/http/middleware"
	"shop-services/internal/model"
	"shop-services/internal/usecase"
	"shop-services/pkg/errs"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type ShopController struct {
	UseCaseShop *usecase.ShopUseCase
	Log         *logrus.Logger
	Config      *viper.Viper
}

func NewShopController(UseCaseShop *usecase.ShopUseCase, log *logrus.Logger) *ShopController {
	return &ShopController{
		Log:         log,
		UseCaseShop: UseCaseShop,
	}
}

func (c *ShopController) CreateShop(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetShopDetailResponse]{
		Status: "FAILED",
	}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)

	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}
	request := new(model.CreateShopRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseShop.CreateShop(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Data = response
	finalResult.Status = "SUCCESS"
	return ctx.JSON(finalResult)
}

func (c *ShopController) UpdateShop(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetShopDetailResponse]{Status: "FAILED"}
	request := new(model.UpdateShopRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}
	response, err := c.UseCaseShop.UpdateShop(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *ShopController) DeleteShop(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[bool]{Status: "FAILED"}
	request := new(model.DeleteShopRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseShop.DeleteShop(ctx, request)
	if response && err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *ShopController) GetListShop(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*[]model.GetShopDetailResponse]{Status: "FAILED"}

	var (
		limitFilter  *int = nil
		offsetFilter *int = nil
	)
	limit := ctx.Query("limit")
	offset := ctx.Query("offset")

	if len(limit) > 0 {
		limitStr, errlimit := strconv.Atoi(limit)
		if errlimit != nil {
			c.Log.WithError(errlimit)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Limit Params")
			return ctx.JSON(finalResult)
		}
		limitFilter = &limitStr
	}
	if len(offset) > 0 {
		offsetStr, erroffset := strconv.Atoi(offset)
		if erroffset != nil {
			c.Log.WithError(erroffset)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Offset Params")
			return ctx.JSON(finalResult)
		}
		offsetFilter = &offsetStr
	}

	orderBy := ctx.Query("orderBy")
	order := ctx.Query("order")
	search := ctx.Query("search")

	response, err := c.UseCaseShop.GetListShop(ctx, &model.GetListParamsRequest{
		Search:  search,
		Order:   order,
		OrderBy: orderBy,
		Limit:   limitFilter,
		Offset:  offsetFilter,
	})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}

	finalResult.Status = "SUCCESS"
	finalResult.Data = &response
	return ctx.JSON(finalResult)
}

func (c *ShopController) GetDetailShop(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetShopDetailResponse]{Status: "FAILED"}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}
	userId := ctx.Params("shop_id")
	response, err := c.UseCaseShop.GetDetailShop(ctx, &model.GetShopDetailRequest{ID: userId})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}
