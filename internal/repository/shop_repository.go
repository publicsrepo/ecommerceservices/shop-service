package repository

import (
	"shop-services/internal/entity"
	"shop-services/internal/model/payload"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type ShopRepository struct {
	Repository[entity.Shop]
	Log *logrus.Logger
}

func NewShopRepository(log *logrus.Logger) *ShopRepository {
	return &ShopRepository{
		Log: log,
	}
}

func (r *ShopRepository) GetListShop(db *gorm.DB, payloads payload.GetListShop) ([]entity.Shop, error) {
	Shop := []entity.Shop{}
	q := db.Where("deleted_at is NULL")

	if len(payloads.Id) > 0 {
		q = q.Where("id in (?)", payloads.Id)
	}
	if len(payloads.Category) > 0 {
		q = q.Where("category in (?)", payloads.Category)
	}
	if len(payloads.ShopId) > 0 {
		q = q.Where("shop_id in (?)", payloads.ShopId)
	}
	err := q.Find(&Shop).Error
	if err != nil {
		return nil, err
	}
	return Shop, nil
}
