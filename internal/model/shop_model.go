package model

// response
type GetShopDetailResponse struct {
	BaseResponse
	Name        string  `json:"name"`
	Category    string  `json:"category"`
	Description *string `json:"description"`
	Logo        string  `json:"logo"`
	Phone       string  `json:"phone"`
	Country     string  `json:"country"`
	Address     string  `json:"address"`
	City        string  `json:"city"`
}

// request
type CreateShopRequest struct {
	Name        string  `json:"name" validate:"required"`
	Category    string  `json:"category" validate:"required"`
	Description *string `json:"description"`
	Logo        string  `json:"logo" validate:"required"`
	Phone       string  `json:"phone" validate:"required"`
	Country     string  `json:"country" validate:"required"`
	Address     string  `json:"address"`
	City        string  `json:"city"`
}

type UpdateShopRequest struct {
	ID          string  `json:"id" validate:"required,uuid"`
	Name        string  `json:"name" validate:"required"`
	Category    string  `json:"category" validate:"required"`
	Description *string `json:"description"`
	Logo        string  `json:"logo" validate:"required"`
	Phone       string  `json:"phone" validate:"required"`
	Country     string  `json:"country" validate:"required"`
	Address     string  `json:"address"`
	City        string  `json:"city"`
}

type DeleteShopRequest struct {
	Ids []string `json:"shop_ids" validate:"required"`
}

type GetShopDetailRequest struct {
	ID string `json:"shop_ids" validate:"required,max=100"`
}
