package transformers

import (
	"shop-services/internal/entity"
	"shop-services/internal/model"
)

func GetShopDetailToResponse(shop *entity.Shop) *model.GetShopDetailResponse {
	result := model.GetShopDetailResponse{}
	if shop == nil {
		return nil
	}

	result.BaseResponse = model.BaseResponse{
		ID:        shop.Id.String(),
		CreatedAt: shop.CreatedAt,
		UpdatedAt: *shop.UpdatedAt,
	}
	result.Name = shop.Name
	result.Category = shop.Category
	result.Description = shop.Description
	result.Logo = shop.Logo
	result.Phone = shop.Phone
	result.Country = shop.Country
	result.Address = shop.Address
	result.City = shop.City
	return &result
}

func GetShopListToResponse(shop []entity.Shop) []model.GetShopDetailResponse {
	finalResult := []model.GetShopDetailResponse{}

	for _, s := range shop {
		finalResult = append(finalResult, model.GetShopDetailResponse{
			BaseResponse: model.BaseResponse{
				ID:        s.Id.String(),
				CreatedAt: s.CreatedAt,
				UpdatedAt: *s.UpdatedAt,
			},
			Name:        s.Name,
			Category:    s.Category,
			Description: s.Description,
			Logo:        s.Logo,
			Phone:       s.Phone,
			Country:     s.Country,
			Address:     s.Address,
			City:        s.City,
		})
	}
	return finalResult
}
