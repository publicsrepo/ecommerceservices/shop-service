package model

import (
	"shop-services/pkg/errs"
	"time"
)

type BaseResponse struct {
	ID        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

type BaseRequest struct {
	ID        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type WebResponse[T any] struct {
	Status string        `json:"status"`
	Data   T             `json:"data,omitempty"`
	Paging *PageMetadata `json:"paging,omitempty"`
	Errors *errs.Errs    `json:"errors,omitempty"`
}

type PageResponse[T any] struct {
	Data         []T          `json:"data,omitempty"`
	PageMetadata PageMetadata `json:"paging,omitempty"`
}

type PageMetadata struct {
	Page      int   `json:"page"`
	Size      int   `json:"size"`
	TotalItem int64 `json:"total_item"`
	TotalPage int64 `json:"total_page"`
}

type GetListParamsRequest struct {
	Search  string `json:"search" validate:"max=30"`
	Order   string `json:"order"`
	OrderBy string `json:"orderBy"`
	Limit   *int   `json:"limit"`
	Offset  *int   `json:"offset"`
}
