package payload

import (
	"shop-services/internal/model"

	"github.com/google/uuid"
)

type GetListShop struct {
	Id       []uuid.UUID
	Category []string
	ShopId   []uuid.UUID
	model.GetListParamsRequest
}
