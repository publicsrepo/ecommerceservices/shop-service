package utils

import "shop-services/internal/model"

type Duration struct {
	Second  int64
	Minutes int64
	Hour    int64
}

type JwtConfig struct {
	Iss       string
	SecretKey string
	Duration  Duration
}

type TokenClaimsData struct {
	Sub   string `json:"sub"`
	Email string `json:"email"`
	Name  string `json:"name"`
	Phone string `json:"phone"`
	Iss   string `json:"iss"`
	Token string `json:"token"`
	Type  string `json:"type"`
	Exp   int    `json:"exp"`
	Aud   string `json:"aud"`
}

type UserDataJwtToToken struct {
	model.BaseResponse
	Email string  `json:"email"`
	Name  string  `json:"first_name"`
	Token *string `json:"token,omitempty"`
	Phone string  `json:"phone"`
}
