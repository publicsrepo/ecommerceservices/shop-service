package entity

type Shop struct {
	BaseEntity
	Name        string  `gorm:"column:name"`
	Category    string  `gorm:"column:category"`
	Description *string `gorm:"column:description"`
	Logo        string  `gorm:"column:logo"`
	Phone       string  `gorm:"column:phone"`
	Country     string  `gorm:"column:country"`
	Address     string  `gorm:"column:address"`
	City        string  `gorm:"column:city"`
}

func (a *Shop) TableName() string {
	return "shop"
}
