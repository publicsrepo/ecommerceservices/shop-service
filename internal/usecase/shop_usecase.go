package usecase

import (
	"fmt"
	"shop-services/internal/entity"
	"shop-services/internal/gateway/httpcall"
	"shop-services/internal/model"
	"shop-services/internal/model/payload"
	"shop-services/internal/model/transformers"
	"shop-services/internal/repository"
	"shop-services/pkg/errs"
	"shop-services/pkg/helper"
	helpers "shop-services/pkg/helper"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type ShopUseCase struct {
	DB             *gorm.DB
	Log            *logrus.Logger
	Validate       *validator.Validate
	ShopRepository *repository.ShopRepository
	Config         *viper.Viper
	//ShopProducer   *messaging.ShopProducer
}

func NewShopUseCase(db *gorm.DB, logger *logrus.Logger, validate *validator.Validate, Config *viper.Viper, shopRepository *repository.ShopRepository) *ShopUseCase {
	return &ShopUseCase{
		DB:             db,
		Log:            logger,
		Validate:       validate,
		ShopRepository: shopRepository,
		Config:         Config,
	}
}

func (c *ShopUseCase) CreateShop(fiberCtx *fiber.Ctx, request *model.CreateShopRequest) (*model.GetShopDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()

	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	httpcall.GetProductDetail(fiberCtx)
	err := c.Validate.Struct(request)
	if err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	now := helper.GetCurrentTimeWithUTC()
	shop := &entity.Shop{
		BaseEntity: entity.BaseEntity{
			Id:        uuid.New(),
			CreatedAt: now,
			UpdatedAt: nil,
			DeletedAt: nil,
		},
		Name:        request.Name,
		Category:    request.Category,
		Description: request.Description,
		Logo:        request.Logo,
		Phone:       request.Phone,
		Country:     request.Country,
		Address:     request.Address,
		City:        request.City,
	}

	if err := c.ShopRepository.Create(tx, shop); err != nil {
		desc := fmt.Sprintf("Failed create shop to database : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetShopDetailToResponse(shop), nil
}

func (c *ShopUseCase) UpdateShop(fiberCtx *fiber.Ctx, request *model.UpdateShopRequest) (*model.GetShopDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	shop := new(entity.Shop)
	if err := c.ShopRepository.FindById(tx, shop, idParsed); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}
	now := time.Now()

	shop.Name = request.Name
	shop.Category = request.Category
	shop.Description = request.Description
	shop.Logo = request.Logo
	shop.Phone = request.Phone
	shop.Country = request.Country
	shop.Address = request.Address
	shop.City = request.City
	shop.UpdatedAt = &now

	if err := c.ShopRepository.Update(tx, shop); err != nil {
		desc := fmt.Sprintf("Failed save shop : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction: %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetShopDetailToResponse(shop), nil
}

func (c *ShopUseCase) DeleteShop(fiberCtx *fiber.Ctx, request *model.DeleteShopRequest) (bool, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idsUuid, errIdParsed := helpers.ManyStringToUUID(request.Ids)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(*errIdParsed)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	shops, err := c.ShopRepository.GetListShop(tx, payload.GetListShop{Id: idsUuid})
	if err != nil {
		desc := fmt.Sprintf("Failed find shop by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	timeNow := helper.GetCurrentTimeWithUTC()
	for _, u := range shops {
		u.DeletedAt = &timeNow
		if err := c.ShopRepository.Update(tx, &u); err != nil {
			desc := fmt.Sprintf("Failed update shop : %+v", err)
			c.Log.WithError(err).Warnf(desc)
			return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
				SetMessage(fiber.StatusInternalServerError).
				SetDescription(desc)
		}
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)

	}

	return true, nil
}

func (c *ShopUseCase) GetDetailShop(fiberCtx *fiber.Ctx, request *model.GetShopDetailRequest) (*model.GetShopDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	uuidParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	shop := entity.Shop{}
	if err := c.ShopRepository.FindById(tx, &shop, uuidParsed); err != nil {
		desc := fmt.Sprintf("Failed find shop by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetShopDetailToResponse(&shop), nil
}

func (c *ShopUseCase) GetListShop(fiberCtx *fiber.Ctx, request *model.GetListParamsRequest) ([]model.GetShopDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	shops, err := c.ShopRepository.GetListShop(tx, payload.GetListShop{
		GetListParamsRequest: *request,
	})
	if err != nil {
		desc := fmt.Sprintf("Failed find shop by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetShopListToResponse(shops), nil
}
