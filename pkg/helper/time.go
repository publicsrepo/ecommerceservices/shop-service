package helper

import "time"

func GetCurrentTimeWithUTC() time.Time {
	return time.Now().UTC()
}
