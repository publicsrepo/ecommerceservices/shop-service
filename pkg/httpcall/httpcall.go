package httpcall

import (
	"encoding/json"
	"shop-services/internal/constant"

	"github.com/gofiber/fiber/v2"
)

func SendRequest(ctx *fiber.Ctx, request Request) error {
	agent := fiber.Agent{}
	switch request.Method {
	case fiber.MethodGet:
		agent = *fiber.Get(request.Url)
	case fiber.MethodPost:
		agent = *fiber.Post(request.Url)
	case fiber.MethodPut:
		agent = *fiber.Put(request.Url)
	case fiber.MethodDelete:
		agent = *fiber.Delete(request.Url)
	}

	if len(request.ContentType) > 0 {
		agent.ContentType(request.ContentType)
	}

	//set header
	agent.Set("Authorization", "Bearer "+request.Token)
	if request.Headers != nil {
		for key, value := range *request.Headers {
			agent.Set(key, value)
		}
	}
	agent.ContentType(constant.ContentTypeApplicationJson)

	statusCode, body, errs := agent.Bytes()
	if len(errs) > 0 {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"errs": errs,
		})
	}
	var bodyResponse fiber.Map
	errUnmarshal := json.Unmarshal(body, &bodyResponse)
	if errUnmarshal != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"err": errUnmarshal,
		})
	}
	return ctx.Status(statusCode).JSON(bodyResponse)
}
