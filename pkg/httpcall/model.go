package httpcall

type Request struct {
	Url         string
	Method      string
	Payload     []byte
	Headers     *map[string]string
	ContentType string
	Token       string
}
