package jwt

import (
	"github.com/golang-jwt/jwt/v5"
)

type CustomClaims struct {
	jwt.RegisteredClaims
	Foo string `json:"foo"`
	Bar string `json:"bar"`
}

func ValidateToken() {
	// authToken := bc.Request().Header.Get("Authorization")

	// token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
	// 	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
	// 		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	// 	}

	// 	return []byte("your_secret_key"), nil
	// })

	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }

	// claims, ok := token.Claims.(*CustomClaims)
	// if !ok {
	// 	fmt.Println("Invalid token claims")
	// 	return
	// }

	// if !token.Valid {
	// 	fmt.Println("Invalid token")
	// 	return
	// }

	// fmt.Println(claims.Foo, claims.RegisteredClaims.ExpiresAt)
}
