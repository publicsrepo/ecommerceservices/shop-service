package errs

const (
	DatabaseError       = 412
	InvalidToken        = 401
	NotFound            = 404
	InternalServerError = 500
)

var (
	// MessageByCode is mapping message to masking original error message.
	MessageByCode = map[int]string{
		404: "NOT_FOUND",
		403: "FORBIDDEN",
		412: "DATABASE_ERROR",
		500: "INTERNAL_SERVER_ERROR",
	}
)

var (
	// ReasonByCode is mapping message to masking original error message.
	ReasonByCode = map[string]string{
		"200": "Success",
		"201": "Created",
		"400": "Bad Request",
		"403": "Forbidden",
		"401": "Invalid Access Token",
		"404": "Not Found",
		"409": "Conflict",
		"412": "Database Error",
		"500": "Internal Server Error",
	}
)

type Errs struct {
	Code        string `json:"code,omitempty"`
	Message     string `json:"message,omitempty"`
	Description string `json:"description,omitempty"`
}
