package errs

import (
	"github.com/gofiber/fiber/v2"
)

type IErrs interface {
	SetMessage(code int) *Errs
	SetDescription(desc string) *Errs

	Log(func(...interface{})) *Errs
}

func (e *Errs) SetMessage(code int) *Errs {
	e.Message = fiber.ErrBadRequest.Message
	return e
}
func (e *Errs) SetDescription(desc string) *Errs {
	e.Description = desc
	return e
}
func (e *Errs) Log(logger func(...interface{})) *Errs {
	logger(e)
	return e
}

func NewErrContext(ctx *fiber.Ctx, code int) IErrs {
	obj := &Errs{}
	ctx.Status(code)
	return obj
}
